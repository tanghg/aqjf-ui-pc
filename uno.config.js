import { defineConfig, presetUno, presetIcons } from "unocss";

export default defineConfig({
  shortcuts: {
    "flex-center": "flex justify-center items-center",
  },
  presets: [
    presetUno(),
    presetIcons({
      warn: true,
      extraProperties: {
        display: "inline-block",
      },
    }),
  ],
});
