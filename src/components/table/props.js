export default {
  /** 数据列 */
  columns: {
    type: Array,
    required: true,
  },
  /** 数据源 */
  data: {
    type: Array,
    required: true,
  },
  /** 序号
   * selection 多选
   * index 序号
   */
  type: {
    type: String,
    default: "",
  },
  /** 操作列 */
  operation: {
    type: Boolean,
    default: true,
  },
};
