export default {
  /** 当前页 */
  page: {
    type: Number,
    required: true,
  },
  /** 每页显示条数 */
  size: {
    type: Number,
    required: true,
  },
  /** 总条数 */
  total: {
    type: Number,
    required: true,
  },
  /** 切换每页显示数量 */
  sizes: {
    type: Array,
    default: [10, 20, 50, 100],
  },
  /** 组件样式 */
  layout: {
    type: String,
    default: "total, sizes, prev, pager, next, jumper",
  },
  /** 切换页数 */
  pageChange: {
    type: Function,
    default: () => {},
  },
  /** 切换每页条数 */
  sizeChange: {
    type: Function,
    default: () => {},
  },
};
