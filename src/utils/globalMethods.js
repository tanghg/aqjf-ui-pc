/**
 * 全局公共方法，需要时引入
 */
import dayjs from "dayjs";

/**
 * 重置form表单
 * @param key 类型keu
 * @returns {string}
 */
export function resetForm(formEl) {
  console.log(formEl.resetFields);
  if (!formEl) return;
  formEl.resetFields();
}

/**
 * 日期格式化
 * @param date 格式化的日期
 * @param type 格式化的类型  默认YYYY-MM-DD
 * @returns {string}
 */
export function dateFormat(date, type) {
  const dateStr = dayjs(date).format(type || "YYYY-MM-DD");
  if (dateStr == "Invalid Date") return "-";
  return dateStr;
}
