import axios from "axios";
import qs from "qs";
import { useStore } from "@/store/store";
import messageBox from "@/utils/messageBox";
function Http(options) {
  let _axios = axios.create({
    baseURL: options.baseURL,
  });

  // _axios.defaults.headers.post["Content-Type"] = "application/json;charset=utf-8";
  // _axios.defaults.headers.put["Content-Type"] = "application/json;charset=utf-8";

  //请求前拦截器，附加令牌
  _axios.interceptors.request.use(
    (config) => {
      // app.config.globalProperties.$showLoading();
      if (config.url != "/v1/auth/login") {
        config.headers.Authorization = useStore().token;
      }
      return config;
    },
    (error) => {
      // app.config.globalProperties.$hideLoading();
      return Promise.reject(error);
    }
  );

  // 响应前拦截器
  _axios.interceptors.response.use(
    (response) => {
      // app.config.globalProperties.$hideLoading();
      const data = response.data;
      if (data.code == "200") {
        // location.href = "/404";
        return response.data;
      } else if (data.code == "400" || data.code == "401" || data.code == "403" || data.code == "500") {
        // error(data.msg || data.message);
        messageBox.alert(data.msg, "error");
        return Promise.reject();
      } else {
        return Promise.reject(data.msg);
      }
    },
    (err) => {
      // error("网络连接异常！");
      // app.config.globalProperties.$hideLoading();
    }
  );

  this._axios = _axios;
}

Http.prototype.post = function (url, params, config) {
  return this._axios.post(url, params, config);
};

Http.prototype.get = function (url, params, config) {
  const config_ = Object.assign({}, config, {
    // 参数
    params,
    // 修改参数序列化方法
    paramsSerializer: {
      serialize: (params) => {
        return qs.stringify(params, { arrayFormat: "repeat" });
      },
    },
    // paramsSerializer: (p) => {
    //   // 使用逗号分隔参数
    //   return qs.stringify(p, {
    //     allowDots: true,
    //   });
    // },
  });
  return this._axios.get(url, config_);
};

Http.prototype.delete = function (url, params, config) {
  const config_ = Object.assign({}, config, {
    // 参数
    params,
    // 修改参数序列化方法
    paramsSerializer: (p) => {
      // 使用逗号分隔参数
      return qs.stringify(p, {
        allowDots: true,
      });
    },
  });
  return this._axios.delete(url, config_);
};

Http.prototype.put = function (url, params, config) {
  return this._axios.put(url, params, config);
};

export default () => {
  //解析接口地址
  // let { url, orgId } = qs.parse(window.location.hash.split('?')[1])

  // if (!orgId) {
  //   orgId = localStorage.getItem('orgId')
  // } else {
  //   localStorage.setItem('orgId', orgId)
  // }

  // if (!url) {
  //   url = localStorage.getItem('url')
  // } else {
  //   localStorage.setItem('url', url)
  // }

  // store.commit('setOrgId', orgId)

  //创建http实例并绑定到全局
  window.$http = new Http({ baseURL: import.meta.env.VITE_BASE_URL });
};
