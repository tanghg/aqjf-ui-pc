import { ElMessage, ElMessageBox } from "element-plus";

/**
 * 消息确认弹出框
 * @param key 类型keu
 * @returns {string}
 */
const messageBox = {
  confirm: (title, content, successCallback, failCallback) => {
    ElMessageBox.confirm(content, title, {
      confirmButtonText: "确认",
      cancelButtonText: "取消",
      type: "warning",
    })
      .then(() => {
        if (typeof successCallback === "function") {
          successCallback();
        }
      })
      .catch(() => {
        if (typeof failCallback === "function") {
          failCallback();
        }
      });
  },
  alert: (msg, type) => {
    ElMessage({
      showClose: true,
      message: msg,
      type: type,
    });
  },
};

export default messageBox;
