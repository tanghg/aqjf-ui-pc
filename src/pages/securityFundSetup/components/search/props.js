export default {

    // 提示内容
    tipstext: {
        type: String,
        default: ''
    },
    // 清空按钮
    refreshLeft: {
        type: Boolean,
        default: true
    },
    // 搜索
    refreshName: {
        type: Function,
        default: () => {},
    },
    //重置
    setupReset: {
        type: Function,
        default: () => {},
    },
    // 绑定的值
    unitName: {
        type: String,

    }
}