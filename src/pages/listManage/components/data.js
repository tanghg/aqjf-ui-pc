export const list1 = [
  {
    name: "细目名称1",
    field: "firstName",
    status: "1",
  },
  {
    name: "细目名称2",
    field: "twoName",
    status: "1",
  },
  {
    name: "细目名称3",
    field: "threeName",
    status: "1",
  },
  {
    name: "费用明细名称",
    field: "minPrice",
    status: "1",
  },
  {
    name: "指导价",
    field: "minPrice",
    status: "1",
  },
  {
    name: "计量单位",
    field: "minPrice",
    status: "1",
  },
  {
    name: "状态",
    field: "minPrice",
    status: "1",
  },
  {
    name: "计量方式",
    field: "minPrice",
    status: "1",
  },
  {
    name: "示意图",
    field: "ids",
    status: "1",
  },
  {
    name: "备注",
    field: "minPrice",
    status: "1",
  },
];

export const list2 = [
  {
    id: "1",
    firstName: "1.1洞口、河流、基坑、泥浆池、作业通道、作业平台等危险部位的安全防护、防滑设施；如：防护栏杆、隔离栅、安全网等，装配式作业通道平台购置费用",
    twoName: "1.1洞口、河流、基坑、泥浆池、作业通道、作业平台等危险部位的安全防护、防滑设施；如：防护栏杆、隔离栅、安全网等，装配式作业通道平台购置费用",
    threeName: "4",
    name: "目录第一层级第一个明细",
    maxPrice: 25.95,
    minPrice: 15.28,
    unitMeasure: "个",
    unitMeasure: "个",
    unitMeasure: "个",
    unitMeasure: "个",
    ids: [],
    remark: "个",
  },
  {
    id: "2",
    firstName: "87",
    twoName: "4",
    threeName: "4",
    name: "目录第一层级第一个明细",
    maxPrice: 25.95,
    minPrice: 15.28,
    unitMeasure: "个",
    unitMeasure: "个",
    unitMeasure: "个",
    unitMeasure: "个",
    ids: [],
    remark: "个",
  },
];
