import { createApp } from "vue";
import "@unocss/reset/tailwind.css";
import "virtual:uno.css";
import App from "./App.vue";
import router from "./router";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import zhCn from "element-plus/dist/locale/zh-cn.mjs";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import "./assets/elementPlus.css";
import "./assets/base.css";
import Directives from "./directives";
import http from "./utils/http"; //http配置
import store from "./store";
import { Dialog } from "vant";

const app = createApp(App);

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

app.use(router).use(ElementPlus, { locale: zhCn }).use(Directives).use(http).use(store).use(Dialog).mount("#app");
