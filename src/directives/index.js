import has from "./has";
import positive from "./positive-integer";

const directives = [has, positive];

export default (app) => {
  directives.forEach((m) => {
    app.directive(`${m.name}`, m.directive);
  });
};
