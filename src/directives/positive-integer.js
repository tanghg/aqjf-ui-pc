// /**
//  * 限制输入框只能输入正整数
//  */
export default {
  name: "positive-integer",
  directive: {
    mounted: function (el, _, vnode) {
      const input = el.getElementsByTagName("input")[0];
      if (input) {
        // 调用input方法，加入正则校验的逻辑
        input.onkeyup = function (e) {
          console.log("keyup", input.value, input.value.replace(/[^\d]/g, ""));
          if (input.value.length === 1) {
            input.value = input.value.replace(/[^0-9]/g, "");
          } else {
            input.value = input.value.replace(/[^\d]/g, "");
          }
          // 调用自定义事件
          trigger(el, "input");
          console.log(vnode);
        };
        input.onblur = function (e) {
          console.log("onblur");
          if (input.value.length === 1) {
            input.value = input.value.replace(/[^0-9]/g, "");
          } else {
            input.value = input.value.replace(/[^\d]/g, "");
          }
          trigger(el, "input");
        };
      }
    },
  },
};

const trigger = (el, type) => {
  console.log("trigger");
  const e = document.createEvent("HTMLEvents");
  // 初始化默认值
  e.initEvent(type, true, true);
  // 触发自定义事件
  el.dispatchEvent(e);
};

// /**
//  * 限制输入框只能输入正整数
//  */
// export default {
//   name: "positive-integer",
//   directive: {
//     beforeMount(dom, binding, vnode) {
//       const el = dom.getElementsByTagName("input")[0];
//       // 为输入框绑定事件处理函数
//       el.addEventListener("input", () => {
//         console.log(el.value);
//         if (new RegExp(/[^\d]/g).test(el.value)) {
//           el.value = el.value.replace(/[^\d]/g, "");
//           trigger(el, "input");
//         }
//       });
//     },
//   },
// };
