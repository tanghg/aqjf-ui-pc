import { defineStore } from "pinia";

export const useStore = defineStore({
  id: "aqjf-task",
  persist: true, //开启数据持久化，所有store数据将被持久化到指定仓库
  // persist: {   //自定义数据持久化方式
  //   key: 'store-key',
  //   storage: window.sessionStorage,
  //   paths: ['token'],
  //   beforeRestore: context => {
  //     // console.log('Before' + context)
  //   },
  //   afterRestore: context => {
  //       // console.log('After'+ context)
  //   }
  // },
  state: () => ({
    token: null,
    userInfo: {},
  }),
  actions: {
    updateToken(val) {
      this.token = val;
    },
    updateUserInfo(val) {
      this.userInfo = val;
    },
  },
});
