import { createRouter, createWebHistory } from "vue-router";

const routersFiles = import.meta.glob("./module/*.js", { eager: true });
let routes = [];
for (const key in routersFiles) {
  routes.push(...routersFiles[key]["default"]);
}

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
