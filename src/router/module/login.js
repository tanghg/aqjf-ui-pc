import Index from "../../pages/login/index.vue";

export default [
  {
    path: "/",
    redirect: "/login",
  },
  {
    path: "/login",
    name: "Index",
    component: Index,
  },
];
