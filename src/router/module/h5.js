import Index from "../../pages/h5/index.vue";

export default [
  {
    path: "/h5",
    redirect: "/h5/index",
  },
  {
    path: "/h5/index",
    name: "H5Index",
    component: Index,
  },
];
