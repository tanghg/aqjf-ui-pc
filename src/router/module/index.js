import Index from "../../pages/index.vue";

export default [
  {
    path: "/index",
    name: "Index",
    component: Index,
  },
];
