export const listCostQuery = (data) => {
  return $http.post("/safe/list/cost/query", data);
};

export const listQuery = (projectId) => {
  return $http.get("/safe/list/query", { projectId });
};

//新增费用明细
export const costAddBatch = (data) => {
  return $http.post("/safe/list/cost/add/batch", data);
};

//目录新增
export const listAdd = (data) => {
  return $http.post("/safe/list/add", data);
};

//目录修改
export const listUpdate = (data) => {
  return $http.post("/safe/list/update", data);
};

//目录删除
export const listDelete = (id) => {
  return $http.get("/safe/list/delete", { id });
};

//目录详情
export const listDetail = (id) => {
  return $http.get("/safe/list/get", { id });
};

//明细详情
export const listCostDetail = (id) => {
  return $http.get("/safe/list/cost/get", { id });
};

//明细修改
export const listCostUpdate = (data) => {
  return $http.post("/safe/list/cost/update", data);
};

//明细删除
export const listCostDelete = (data) => {
  return $http.post("/safe/list/cost/delete", data);
};

//申请新增费用明细
export const listApplyAdd = (data) => {
  return $http.post("/safe/list/apply/add", data);
};

//查询已提交的申请新增费用明细
export const listApplyQuery = (data) => {
  return $http.post("/safe/list/apply/query", data);
};

//审核通过
export const listApplyAudit = (data) => {
  return $http.post("/safe/list/apply/audit", data);
};

//显示和隐藏目录
export const listHideUpdate = (data) => {
  return $http.post("/safe/list/hide/update", data);
};
