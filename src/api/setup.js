const urls = {
    List: "/org/query", //获取单位列表
    unitData: "/org/get", //获取单位信息详情
    unitUpdate: "/org/update", //更新单位信息
    unitRoleadd: "/role/add", //新增角色信息
    unitRoleUpdet: "/role/update", //角色修改
    unitRoleList: "/role/query", //角色列表
    unitRoleBind: 'user/role/bind', //把角色绑定给人
    unitBind: "/role/permission/bind", //给角色绑定权限
    unitPermission: "/role/permission", //查角色权限所有权限列表
    unitRolebindList: "/user/query/bind", //角色已绑定的权限
    unitRoleListOwn: 'permission/query/tree', //查询所有的权限
    unitRolebrienameList: "/org/project/query", //角色绑定用户查询项目下所有机构
    unitRolebDelete: "/role/delete", //删除角色
};
const unitList = (data) => {
    return $http.post(urls.List, data);
};
const unitData = (data) => {
    return $http.get(urls.unitData, data);
};
const unitUpdate = (data) => {
    return $http.post(urls.unitUpdate, data);
};
const unitRoleadd = (data) => {
    return $http.post(urls.unitRoleadd, data);
};
const unitRoleList = (data) => {
    return $http.post(urls.unitRoleList, data);
};
const unitRoleUpdet = (data) => {
    return $http.post(urls.unitRoleUpdet, data);
};

const unitRolebindList = (data) => {
    return $http.post(urls.unitRolebindList, data);
};
const unitRolebrienameList = (data) => {
    return $http.get(urls.unitRolebrienameList, data);
};
const unitBind = (data) => {
    return $http.post(urls.unitBind, data);
};
const unitRolebDelete = (data) => {
    return $http.get(urls.unitRolebDelete, data);
};
const unitPermission = (data) => {
    return $http.get(urls.unitPermission, data);
};
const unitRoleListOwn = (data) => {
    return $http.get(urls.unitRoleListOwn, data);
};

const unitRoleBind = (data) => {
    return $http.post(urls.unitRoleBind, data);
};
export { unitList, unitData, unitUpdate, unitRoleadd, unitRoleList, unitRoleUpdet, unitRolebindList, unitRolebrienameList, unitBind, unitRolebDelete, unitPermission, unitRoleListOwn, unitRoleBind };