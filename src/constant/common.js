// 清单计量方式
export const MEASURE_TYPE = [
  { value: "1", label: "清单单价计量" },
  { value: "2", label: "据实计量" },
  { value: "3", label: "总价包干" },
];

// 清单计量方式对象
export const MEASURE_TYPE_OBJ = {
  1: "清单单价计量",
  2: "据实计量",
  3: "总价包干",
};

//数据状态
export const STATUS_TYPE = [
  { value: "1", label: "待申请" },
  { value: "2", label: "审核中" },
  { value: "3", label: "已审核" },
  { value: "4", label: "已驳回" },
];

export const STATUS_TYPE_OBJ = {
  1: "待申请",
  2: "审核中",
  3: "已审核",
  4: "已驳回",
};
