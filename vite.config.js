import { fileURLToPath, URL } from "node:url";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { VantResolver } from "unplugin-vue-components/resolvers";
import UnoCSS from "unocss/vite";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    UnoCSS(),
    AutoImport({
      imports: ["vue"],
      dts: "src/auto-import.d.ts",
    }),
    Components({
      resolvers: [VantResolver()],
    }),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  server: {
    host: "0.0.0.0",
  },
  // proxy: {
  //   "/api": {
  //     target: "http://192.168.10.43:8089/jf", // 目标服务器的URL
  //     changeOrigin: true, // 开启代理，并且会更改请求头中的Origin字段
  //     rewrite: (path) => path.replace(/^\/api/, ""), // 重写请求路径
  //   },
  // },
});
